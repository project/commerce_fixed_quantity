<?php

/**
 * Implements hook_views_data_alter().
 */
function commerce_fixed_quantity_views_data_alter(&$data) {
  $data['commerce_line_item']['edit_quantity'] = array(
    'field' => array(
      'title' => t('Quantity text field'),
      'help' => t('Adds a text field to edit the line item quantity in the View.'),
      'handler' => 'commerce_fixed_quantity_handler_field_quantity',
    ),
  );
}
