<?php

/**
* @file
* Views field handler. Adds the quantity field to the view.
* Implements the Views Form API.
*/

/**
 * Field handler to present a field to change quantity of a line item. Based on
 * commerce_line_item_handler_field_edit_quantity 7.x-1.9
 */
class commerce_fixed_quantity_handler_field_quantity extends commerce_line_item_handler_field_edit_quantity {

  function construct() {
    parent::construct();
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );
    // At this point, the query has already been run, so we can access the results
    // in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {

      $line_item_id = $this->get_value($row, 'line_item_id');
      $quantity = $this->get_value($row, 'quantity');

      $form[$this->options['id']][$row_id] = array(
        '#type' => 'textfield',
        '#datatype' => 'integer',
        '#default_value' => round($quantity),
        '#size' => 4,
        '#maxlength' => max(4, strlen($quantity)),
        '#line_item_id' => $line_item_id,
        '#attributes' => array(
          'title' => $this->options['label'],
        ),
      );

      // Load the product
      $line_item =  commerce_line_item_load($line_item_id);
      $product = entity_metadata_wrapper('commerce_line_item', $line_item)->commerce_product->value();

      $quantityMultipliers = _commerce_fixed_quantity_get_multipliers($product);
      if ($quantityMultipliers) {
        _commerce_fixed_quantity_form_replace($form[$this->options['id']][$row_id], $quantityMultipliers);
      }
    }
  }

  function views_form_validate($form, &$form_state) {
    $field_name = $this->options['id'];
    foreach (element_children($form[$field_name]) as $row_id) {
      _commerce_fixed_quantity_validate($form_state['values'][$field_name][$row_id]['multiplier_quantity'],
        $field_name . '][' . $row_id,
        ($form[$field_name][$row_id]['#datatype'] == 'integer'));
    }
  }

  function views_form_submit($form, &$form_state) {

    $field_name = $this->options['id'];

    foreach (element_children($form[$field_name]) as $row_id) {
      $line_item_id = $form[$field_name][$row_id]['#line_item_id'];

      // If the line item hasn't been deleted...
      if ($line_item = commerce_line_item_load($line_item_id)) {
        $values = &$form_state['values'][$field_name][$row_id];
        $form_quantity = $values['multiplier'] * $values['multiplier_quantity'];
        $values = $form_quantity;
      }
    }

    parent::views_form_submit($form, $form_state);

  }
}
