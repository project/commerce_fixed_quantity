<?php
/**
* @file
* Admin pages
*/

/**
 * Form builder; Configure footer settings
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function commerce_fixed_quantity_admin_settings($form, &$form_state) {

  $defaults = variable_get('commerce_fixed_quantity_factors', array());
  $defaults += array('unit' => array('factor' => 1, 'label' => t('Item')));

  $form['commerce_fixed_quantity_allow_unit'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('commerce_fixed_quantity_allow_unit', TRUE),
    '#title' => t('Allow adding single units to cart'),
  );

  for ($i = 0; $i < 4; $i++) {
    $default = array_shift($defaults);
    $form['group_' . $i] = array(
      '#type' => 'fieldset',
      '#title' => t('Factor') . ' ' . $i,
    );
    $form['group_' . $i]['factor_' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Factor'),
      '#default_value' => $default ? $default['factor'] : '',
    );

    $form['group_' . $i]['factor_' . $i . '_label'] = array(
      '#type' => 'textfield',
      '#default_value' => $default ? $default['label'] : '',
      '#title' => t('Label'),
    );
  }

  // Prevent editig single unit factor
  $form['group_0']['factor_0']['#disabled'] = TRUE;
  $form['group_0']['#title'] = t('Item');

  if (module_exists('token')) {
    $form['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('commerce-product'),
    );
  }
  else {
    $form['token_tree'] = array(
      '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>',
    );
  }

  $form = system_settings_form($form);
  $form['#submit'] = array('_commerce_fixed_quantity_admin_submit');

  return $form;
}

/**
 * Save values
 *
 * @param $form
 * @param $form_state
 */
function _commerce_fixed_quantity_admin_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $quantity_factors = array();
  $quantity_factors['unit'] = array(
    'factor' => 1,
    'label' => check_plain($values['factor_0_label'])
  );

  for ($i = 1; $i < 4; $i++) {
    if ($values['factor_' . $i]) {
      $quantity_factors[] = array(
        'factor' => check_plain($values['factor_' . $i]),
        'label' => check_plain($values['factor_' . $i . '_label'])
      );
    }
  }

  variable_set('commerce_fixed_quantity_factors', $quantity_factors);
  variable_set('commerce_fixed_quantity_allow_unit', $values['commerce_fixed_quantity_allow_unit']);

  // Clear multiplier cache.
  $cache_name = 'commerce_fixed_quantity_get_multipliers_';
  cache_clear_all($cache_name, 'cache', TRUE);
}
